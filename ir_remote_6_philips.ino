//#include <IRremote.h>
#include <RobotIRremoteTools.h>
//#include <avr/delay.h> 

int ON = 1;
int OFF = 2;

int lampOnePin = 2;
int lampTwoPin = 4;
int buttonPin = 8;

int lastButtonState = LOW;

int lampOneState = OFF;
int lampTwoState = OFF;


#define command_1_0 0x1002B
#define command_1_1 0x2B
#define command_2_0 0x10028
#define command_2_1 0x28
#define command_NONE -1
#define command_delay_ms 500

unsigned long lastCommandCode = command_NONE;
unsigned long timeLastCommandRecived;

void setup() { 
  beginIRremote(); // включить приемник
    
//  Serial.begin(9600); 
//  Serial.print("Command 1 code is "); 
  
  pinMode(lampOnePin, OUTPUT);
  pinMode(lampTwoPin, OUTPUT);
  pinMode(buttonPin, INPUT); 
  setLampState(lampOnePin, &lampOneState, OFF);
  setLampState(lampTwoPin, &lampTwoState, OFF);
} 
  
void loop() { 
    if (IRrecived()) {
      unsigned long newCommand = getIRresult();
//      Serial.println(newCommand, HEX);
      unsigned long _millis = millis();
      boolean fGo = false;
      if( isCommand(newCommand) ) {
      
        if(lastCommandCode == command_NONE ) {
            fGo = true;
        } else {
           unsigned long timeDiff = _millis - timeLastCommandRecived;
 //         Serial.print("timeDiff ");
 //         Serial.println(timeDiff);
           if(timeDiff < 0 || timeDiff > command_delay_ms ) {
             if(lastCommandCode != newCommand) {
               fGo = true;
             }
           }
        }
        //Serial.println(getIRresult(), HEX);
        if(fGo) {
           processCommand(newCommand);
  //         delay(50);
           lastCommandCode = newCommand;
           timeLastCommandRecived = _millis;
        }
      }
      resumeIRremote();
    } else {
      int newButtonState = digitalRead(buttonPin);

      if( lastButtonState != newButtonState ) {
        int cnt=0;
        int cntMax=5;

        for(int c=0; c < cntMax; c++) {
          newButtonState = digitalRead(buttonPin);
          if( lastButtonState != newButtonState ) {
            cnt++;
          }
          delay(20);
        }

        if( cnt < (cntMax - 1) ) {
          newButtonState = lastButtonState;
        }
      }

       if(HIGH == newButtonState) {
           if(lastButtonState == LOW) {
              if(lampOneState == ON || lampTwoState == ON) {
                   if(lampOneState == ON) {
                      switchLamp(lampOnePin, &lampOneState);
                   }
                   if(lampTwoState == ON) {
                      switchLamp(lampTwoPin, &lampTwoState);
                   }
              } else {
                   if(lampOneState == OFF) {
                      switchLamp(lampOnePin, &lampOneState);
                   }
                   if(lampTwoState == OFF) {
                      switchLamp(lampTwoPin, &lampTwoState);
                   }
              }
           }
           delay(150);
       }

       if( lampOneState == OFF && lampTwoState == OFF ) {
          setLampState(lampOnePin, &lampOneState, OFF);
          setLampState(lampTwoPin, &lampTwoState, OFF);
       }
       
       lastButtonState = newButtonState;
    }
} 

void processCommand(unsigned long commandCode) {
    switch(commandCode) {
       case command_1_0:
       case command_1_1:
          switchLamp(lampOnePin, &lampOneState);
          break;
       case command_2_0:
       case command_2_1:
          switchLamp(lampTwoPin, &lampTwoState);
          break;
    }
}

boolean isCommand(unsigned long commandCode) {
    switch(commandCode) {
       case command_1_0:
       case command_1_1:
       case command_2_0:
       case command_2_1:
          return true;
          break;
    }
    return false;
}

void switchLamp(int pin, int *stateHolder) {
    if( *stateHolder == OFF ) {
       *stateHolder = ON;
    } else {
       *stateHolder = OFF;
    }

//    Serial.print("state is "); 
//    Serial.print(*stateHolder); 
//    Serial.print(", pin "); 
//    Serial.print(pin); 
//    Serial.print(" switched "); 
    if( *stateHolder == ON ) {
       setLampState(pin, stateHolder, ON);
       //Serial.println("ON"); 
   } else {
       setLampState(pin, stateHolder, OFF);
       //Serial.println("OFF"); 
    }
}

void setLampState(int pin, int *stateHolder, int newState) {
   *stateHolder = newState;
   if(newState == ON) {
      digitalWrite(pin, HIGH);
   } else {
      digitalWrite(pin, LOW);
   }
}

